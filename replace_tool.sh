#!/usr/bin/env bash

regex=${1}
new=${2}

if [[ ${regex} == "" || ${new} == "" ]]
then
    echo "Usage: ${0} <regex> <replacement>"
    echo "Description: Replaces any regex matches with the provided replacement text for all entries."
    exit
fi

shopt -s dotglob

for f in entries/*
do
    if [[ "${OSTYPE}" == "darwin"* ]]
    then
        sed -i "" -e "s/${regex}/${new}/g" "${f}"
    else
        sed -i "s/${regex}/${new}/g" "${f}"
    fi
done
