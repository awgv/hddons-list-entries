from os import path, environ
from subprocess import call
import tempfile
import curses
import editor
from . Entry import Entry
from . Windows import Windows
from . BaseMenu import BaseMenu
from . DependenciesMenu import DependenciesMenu
from . TagsMenu import TagsMenu

class EntryDetailsMenu(BaseMenu):
    def __init__(self, win: Windows, entry: Entry, tags: list):
        super().__init__(win)

        self.win.clear_all()
        self.win.top.addstr("[Select entry attribute]")
        self.win.mid.addstr("Loading entry details...")
        self.win.bot.addstr(editor.DEFAULT_KEYBINDS + "   | [1-9] jump to")
        self.win.refresh_all()
        self.entry = entry
        self.tags = tags
        self.update_lst()

    def update_lst(self) -> None:
        self.lst.clear()
        self.lst.append("Title:        {}".format(self.entry.title))
        self.lst.append("Credits:      {}".format(self.entry.credits))
        self.lst.append("Dependencies: {}".format(self.entry.dependencies))
        self.lst.append("Tags:         {}".format(self.entry.tags))
        self.lst.append("Flairs:       {}".format(self.entry.flairs))
        self.lst.append("Flag:         {}".format(self.entry.flag))
        self.lst.append("Description:\n{}".format(self.entry.description))

    def check_input(self, key: int) -> bool:
        if key == ord('0'):
            key = ord('9')

        if key >= ord('1') and key <= ord('9'):
            self.sel = min(key - ord('1'), len(self.lst) - 1)

        else:
            return super().check_input(key)

        return True

    def accept(self) -> None:
        match self.sel:
            case 0:
                self.entry.title = self.edit_details(self.entry.title)

            case 1:
                self.entry.credits = self.edit_details(self.entry.credits)

            case 2:
                DependenciesMenu(self.win, self.entry).loop()

            case 3:
                TagsMenu(self.win, self.tags, self.entry.tags).loop()
                self.entry.tags.sort()

            case 4:
                self.entry.flairs = {} # this just removes flairs for now, will write the menu next time

            case 6:
                # temporarily exits curses mode (apparrently, this doesn't invalidate all windows?)
                curses.endwin()
                with tempfile.NamedTemporaryFile() as file:
                    file.write(bytes(self.entry.description, 'utf-8'))
                    file.seek(0)
                    editor = environ.get("EDITOR")
                    call([editor, file.name])
                    self.entry.description = file.read().decode('utf-8')

                # return to curses mode (this returns the initial screen that we init with)
                newscr = curses.initscr()
                newscr.refresh()
                curses.doupdate()

        self.update_lst()

    def edit_details(self, details: str) -> str:
        editwin = self.create_floating_win(self.win.mid, (1, -1))
        self.win.mid.refresh()

        return self.create_textbox(editwin, details).strip()
