from . Windows import Windows
from . Entry import Entry
from . BaseMenu import BaseMenu
from . EntriesMenu import EntriesMenu
from . TagsMenu import TagsMenu
from . EntryDetailsMenu import EntryDetailsMenu
from . DependenciesMenu import DependenciesMenu
from . TextEditor import TextEditor
import curses
from ruamel.yaml import YAML
from os import listdir
from os.path import isfile, join

KEY_ENTER = 10
KEY_ESC = 27
KEY_PASTE = 22

DEFAULT_KEYBINDS = "[ESC] back   | [ENTER] accept   | [UP/DOWN] move"
ESC_IGNORE = "[esc_ignore]"

entries = []

def load_entries(win: curses.window) -> None:
    file_entries = [join("entries", f) for f in sorted(listdir("entries")) if isfile(join("entries", f)) and f.endswith(".yaml")]
    entries.clear()
    win.clear()
    win.addstr("Loading entries...\n")
    win.refresh()
    for i in file_entries:
        win.clear()

        with open(i, 'r') as file:
            contents = YAML(typ="safe").load(file.read())
            entries.append(Entry(contents, i))

def replace_unwanted_chars(text):
    text = text.replace(' ', '_')
    text = text.replace('/', '')
    text = text.replace('(', '')
    text = text.replace(')', '')
    text = text.replace('\\', '')
    text = text.replace("'", '')
    text = text.replace('"', '')
    text = text.replace('{', '')
    text = text.replace('}', '')
    text = text.replace('[', '')
    text = text.replace(']', '')
    text = text.replace('~', '')
    text = text.replace('`', '')
    text = text.replace('!', '')
    text = text.replace('@', '')
    text = text.replace('#', '')
    text = text.replace('$', '')
    text = text.replace('%', '')
    text = text.replace('^', '')
    text = text.replace(':', '')
    text = text.replace(';', '')
    text = text.replace('&', "and")
    text = text.replace('*', '')
    text = text.replace('.', '')
    text = text.replace(',', '')
    return text
