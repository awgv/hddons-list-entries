import curses
import pyperclip
import editor
from . Entry import Entry
from . Windows import Windows
from . BaseMenu import BaseMenu
from . TextEditor import TextEditor

class DependenciesMenu(BaseMenu):
    def __init__(self, win: Windows, entry: Entry):
        super().__init__(win)

        self.win = Windows(win.top, self.create_floating_win(win.mid), win.bot)
        self.prevwin = win.mid
        self.search = ""
        self.is_searching = False
        self.load_dependencies(entry)
        self.entry = entry

    def load_dependencies(self, entry):
        if "" in entry.dependencies:
            entry.dependencies.remove("")

        self.lst = ["{} ({})".format(d[1:][:d.find("](") - 1], d[d.find("](") + 2:-1]) for d in entry.dependencies]
        self.lst.append("[+ add dependency +]")

    def accept(self) -> None:
        if self.sel != len(self.lst) - 1:
            return

        self.win.bot.clear()
        self.win.bot.addstr(editor.DEFAULT_KEYBINDS + "   | [^V] use search box text")
        self.win.bot.refresh()
        self.is_searching = True
        self.sel = 0
        self.lst = [e.title for e in editor.entries]
        editwin = curses.newwin(1, self.win.mid.getmaxyx()[1], self.win.mid.getparyx()[0] - 1, self.win.mid.getparyx()[1])

        def validator(box: TextEditor, key: int) -> int:
            if key in (curses.KEY_LEFT, curses.KEY_RIGHT, curses.ascii.ESC):
                return key

            elif key == curses.KEY_UP or key == curses.KEY_DOWN:
                self.check_input(key)

            elif key == editor.KEY_ENTER or key == editor.KEY_PASTE:
                linkwin = self.create_floating_win(self.prevwin, (1, int(self.prevwin.getmaxyx()[1] / 3)))
                self.prevwin.refresh()

                def handle_paste(linkbox: TextEditor, key: int) -> int:
                    if key != editor.KEY_PASTE:
                        return key

                    linkbox.contents = pyperclip.paste()
                    return curses.ascii.NL

                link = self.create_textbox(linkwin, "", handle_paste).strip()
                if link == "":
                    return None

                if len(self.lst) == 0 or key == editor.KEY_PASTE:
                    selected = box.gather().strip()

                else:
                    selected = self.lst[self.sel]

                self.entry.dependencies.append("[{}]({})".format(selected, link))
                return curses.ascii.BEL

            else:
                box.do_command(key)
                self.search = box.gather().strip()
                self.lst = [e.title for e in editor.entries if self.search == "" or e.title.lower().find(self.search.lower()) != -1]
                self.sel = 0
                self.offset = 0

            self.draw()
            box.update_display()
            return None

        self.draw()
        self.create_textbox(editwin, "", validator)
        editwin.clear()
        editwin.refresh()
        self.prevwin.refresh()
        self.load_dependencies(self.entry)
        self.sel = 0
