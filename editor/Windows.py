import curses

class Windows:
    def __init__(self, top, mid, bot):
        self.top = top
        self.mid = mid
        self.bot = bot
        self.mid.keypad(True)
        self.top.bkgd(' ', curses.color_pair(1))
        self.mid.bkgd(' ', curses.color_pair(0))
        self.bot.bkgd(' ', curses.color_pair(1))

    def refresh_all(self) -> None:
        self.top.refresh()
        self.mid.refresh()
        self.bot.refresh()

    def clear_all(self) -> None:
        self.top.clear()
        self.mid.clear()
        self.bot.clear()
